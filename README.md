# luxury

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

Other techs:

[localforage](https://github.com/mozilla/localForage)
[async.js](https://github.com/caolan/async)
[masonry](https://github.com/passy/angular-masonry)
[compass](http://compass-style.org/)
[bootstrap](http://getbootstrap.com/)
[lodash](https://lodash.com/)

steps:

1. npm install
2. bower install
3. grunt serve

you will need:

nodeJS -> nvm (node version manager) recommended
ruby  -> rvm(ruby version manager) recommended

ruby and node just for development purposes


thanks!


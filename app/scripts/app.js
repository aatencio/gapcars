'use strict';

/**
 * @ngdoc overview
 * @name luxuryApp
 * @description
 * # luxuryApp
 *
 * Main module of the application.
 */
angular
  .module('luxuryApp', [
    'ngAnimate',
    'ngRoute',
    'ngTouch',
    'mgcrea.ngStrap',
    'slick',
    'wu.masonry'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/detail/:id', {
        templateUrl: 'views/detail.html',
        controller: 'DetailCtrl'
      })
      .when('/compare/:other/:current', {
        templateUrl: 'views/compare.html',
        controller: 'CompareCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function(cars) {
    /** Sync the json file in the local storage **/
    cars.sync().then(function(synced) {
      if (!synced) {
        console.log('error');
      }
    });
  });

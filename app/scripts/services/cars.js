'use strict';

/**
 * @ngdoc service
 * @name luxuryApp.cars
 * @description
 * # cars
 * Factory in the luxuryApp.
 */

angular.module('luxuryApp')
  .factory('cars', function($q, $http) {
    return {
      sync: function() {
        var defer = $q.defer();
        $http.get('../json/products.json').success(function(data) {
          localforage.setItem('cars', data, function(err) {
            if (!err) {
              defer.resolve(true);
            } else {
              defer.reject(false);
            }
          });
        }).error(function() {
          defer.reject(false);
        });
        return defer.promise;
      },

      getAll: function() {
        var defer = $q.defer();
        localforage.getItem('cars').then(function(allCars) {
          if (allCars !== null) {
            defer.resolve(_.sortBy(allCars, 'model'));
          } else {
            defer.reject({error:true});
          }
        });
        return defer.promise;
      },

      getById: function(paramId) {
        var defer = $q.defer();
        localforage.getItem('cars').then(function(allCars) {
          if (allCars !== null) {
            defer.resolve(_.where(allCars, {id:paramId}));
          } else {
            defer.reject({error:true});
          }
        });
        return defer.promise;
      },

      find: function(search) {
        var defer = $q.defer();
        localforage.getItem('cars').then(function(allCars) {
          if (allCars !== null) {
            async.parallel({
              model: function(CB) {
                var model = _.where(allCars, {model: search});
                CB(null, model);
              },
              brand: function(CB) {
                var brand = _.where(allCars, {brand: search});
                CB(null, brand);
              }
            }, function(err, results) {
              if (_.isEmpty(results.brand)) {
                defer.resolve(results.model);
              } else {
                defer.resolve(results.brand);
              }
            });
          } else {
            defer.reject({error:true});
          }
        });
        return defer.promise;
      },

      compareById: function(car1, car2) {
        var defer = $q.defer();
        var that = this;
        async.parallel({
          carOne: function(CB) {
            that.getById(car1).then(function(carObj){
              CB(null, carObj);
            });
          },
          carTwo: function(CB) {
            that.getById(car2).then(function(carObj){
              CB(null, carObj);
            });
          }
        }, function(err, results) {
          if (!err) {
            defer.resolve(results);
          } else {
            defer.reject({error:true});
          }
        });
        return defer.promise;
      }
    };
  });

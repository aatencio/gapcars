'use strict';

/**
 * @ngdoc directive
 * @name luxuryApp.directive:menu
 * @description
 * # menu
 */
angular.module('luxuryApp')
  .directive('menu', function(cars, $timeout) {
    return {
      templateUrl: '../views/topmenu.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });

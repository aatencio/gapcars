'use strict';

/**
 * @ngdoc directive
 * @name luxuryApp.directive:finder
 * @description
 * # finder
 */
angular.module('luxuryApp')
  .directive('finder', function(cars, $timeout) {
    return {
      templateUrl: '../views/finder.html',
      restrict: 'E',
      scope:{
        placeHolder:'@',
        compare: '@',
        currentCar: '@'
      },
      link: function postLink(scope, element, attrs) {
        scope.$watch('searchOpts',  _.debounce(function(val) {
          if (val !== undefined) {
            scope.$apply(function() {
              cars.find(val).then(function(searchResult) {
                scope.results = searchResult;
              });
            });
          }
        }, 1000));

        scope.clean = function() {
          scope.results = [];
        };
      }
    };
  });

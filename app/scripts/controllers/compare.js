'use strict';

/**
 * @ngdoc function
 * @name luxuryApp.controller:CompareCtrl
 * @description
 * # CompareCtrl
 * Controller of the luxuryApp
 */
angular.module('luxuryApp')
  .controller('CompareCtrl', function ($scope, cars, $routeParams) {
    cars.compareById($routeParams.other, $routeParams.current).then(function(carsObj) {
      $scope.carsObj = carsObj;
    });
  });

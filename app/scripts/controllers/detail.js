'use strict';

/**
 * @ngdoc function
 * @name luxuryApp.controller:DetailCtrl
 * @description
 * # DetailCtrl
 * Controller of the luxuryApp
 */
angular.module('luxuryApp')
  .controller('DetailCtrl', function($scope, $routeParams, cars) {
    cars.getById($routeParams.id).then(function(car) {
      $scope.car = car[0];
    });
  });

'use strict';

/**
 * @ngdoc function
 * @name luxuryApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the luxuryApp
 */
angular.module('luxuryApp')
  .controller('MainCtrl', function($scope, cars, $location) {
    cars.getAll().then(function(allCars) {
    	console.log(allCars);
      $scope.carList = allCars;
    });
    
    $scope.detail = function(detailPath) {
      $location.path(detailPath);
    }
    
  });

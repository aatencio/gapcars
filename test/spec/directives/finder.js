'use strict';

describe('Directive: finder', function () {

  // load the directive's module
  beforeEach(module('luxuryApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<finder></finder>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the finder directive');
  }));
});
